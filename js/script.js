// requestAnimationFrame Shim
window.requestAnimFrame = (function () {
    return  window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
})();
var canvas = document.getElementById('circle_rotation');
var context = canvas.getContext('2d');
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;

var endPercent = 100;
var curPerc = 0;
var counterClockwise = false;
var circ = Math.PI * 2;
var quart = Math.PI / 2;
context.lineWidth = 1;
context.shadowOffsetX = 0;
context.shadowOffsetY = 0;
context.strokeStyle = '#E8E8E8';
context.shadowBlur = 4;
var radianAngle = 0;

// do animation
animate();

// define functions
function animate(current) {

    var small_radius, big_radius, moving_point_width, moving_point_height, logo_width, logo_height;
    var x = canvas.width / 2;
    var y = canvas.height / 2;
    
    var img = new Image();
    img.src = "moving_point.png";

    var logo = new Image();
    logo.src = "logo.png";

    if (width < 1200) {
        big_radius = x / 3.6;
        small_radius = big_radius - 15;
    } else if ((width < 1200) && (width >= 250)) {
        big_radius = x / 3.8;
        small_radius = big_radius - 30;
    } else {
        big_radius = x / 4.5;
        small_radius = big_radius - 30;
    }

    if (width < 1200) {
        var k = logo.width / logo.height;
        logo_width = 2 * small_radius * 0.75;
        logo_height = parseInt(logo_width / k);
    } else {
        logo_width = logo.width;
        logo_height = logo.height;
    }

// draw big cicrle
    if ((curPerc > 0) && (curPerc < 25)) {
        context.strokeStyle = '#D8D8D8';
        context.shadowColor = '#E8E8E8';
    } else if ((curPerc >= 25) && (curPerc < 50)) {
        context.strokeStyle = '#E5FFF7';
        context.shadowColor = '#CCFFEE';
    } else if ((curPerc >= 50) && (curPerc < 75)) {
        context.shadowColor = '#00CCFF';
        context.strokeStyle = '#33FFFF';
    } else {
        context.strokeStyle = '#33CCCC';
        context.shadowColor = '#16A085';
    }

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.arc(x, y, big_radius, -(quart), ((circ) * current) - quart, false);
    context.stroke();
    // draw small cicrle
    context.shadowColor = '#E8E8E8';
    context.strokeStyle = '#E0E0E0';
    context.beginPath();
    context.arc(x, y, small_radius, -(quart), ((circ) * current) - quart, false);
    context.stroke();
    curPerc++;
    if (curPerc < endPercent) {
        requestAnimationFrame(function () {
            animate(curPerc / 100)
        });
    }

// when curPerc reach max value set it to 0 to can start animation again
    if (curPerc == 99)
        curPerc = 0;
    // moving cirlcle
    radianAngle += Math.PI / 120;
    context.save();
    context.translate(x, y);
    context.rotate(radianAngle);
    context.drawImage(img, small_radius - img.width / 2, -img.height / 2);
    context.restore();
    // draw background line
    var background_line_width = (canvas.width - small_radius * 2) / 2;
    context.beginPath();
    context.moveTo(0, y);
    context.lineTo(background_line_width, y);
    context.stroke();
    context.beginPath();
    context.moveTo(background_line_width + small_radius * 2, y);
    context.lineTo(canvas.width, y);
    context.stroke();
    // draw logo in center
    context.drawImage(logo, x - logo_width / 2, y - logo_height / 2, logo_width, logo_height);
}


